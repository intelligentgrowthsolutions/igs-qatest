﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using QATest.Models;
using QATest.DTOs;
using QATest.Infra;

/*
    There are two deliberate mistakes:
        - when updating a product and a non-existing ID is entered, it returns the code 426
        - when listing the products, one of them has its name duplicated twice

    
    Things from the top of my head the person may suggest to improve:
        - required fields (can add an empty product)
        - can add duplicates of the same name
        - put/post price is a double, get is a string (confusing?)
*/

namespace QATest.Controllers
{
    [Route("api")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private FakeRepo _fakeRepo;
        private int _idTracker;

        public ProductController(FakeRepo repo)
        {
            _fakeRepo = repo;
            _idTracker = _fakeRepo.Products.Count;
        }

        /// <summary>
        /// Provides a list of all products.
        /// </summary>
        /// <returns>A list of products.</returns>
        [HttpGet("products")]
        public ActionResult<IEnumerable<DtoProduct>> Get()
        {
            // deliberate mistake, change one of the product names so that its duplicated twice
            List<DtoProduct> products = Mapper.ProductsToDtos(_fakeRepo.Products.ToList());

            DtoProduct change = products.FirstOrDefault(p => p.Id >= 3);

            if(change != null)
                change.Name = $"{change.Name} {change.Name}";

            return products;
        }

        /// <summary>
        /// Gets a product by ID.
        /// </summary>
        /// <param name="id">The ID of the product to return.</param>
        /// <returns>A Product.</returns>
        [HttpGet("product/{id}")]
        public ActionResult<DtoProduct> Get(int id)
        {
            if(!_fakeRepo.Products.Any(p => p.Id == id))
            {
                return NotFound();
            }

            return Mapper.ProductToDto(_fakeRepo.Products.First(p => p.Id == id));
        }

        /// <summary>
        /// Creates a new product.
        /// </summary>
        /// <param name="product">The product to create.</param>
        /// <returns>Http 200 response if successful.</returns>
        [HttpPost("product")]
        public ActionResult Post([FromForm] Product product)
        {
            _idTracker++;
            product.Id = _idTracker;
            _fakeRepo.Products.Add(product);
            return Ok();
        }

        /// <summary>
        /// Updates an existing product.
        /// </summary>
        /// <param name="id">The ID of the product to update.</param>
        /// <param name="product">The product to update.</param>
        /// <returns>Http 200 response if successful.</returns>
        [HttpPut("product/{id}")]
        public ActionResult Put(int id, [FromForm] Product updateProduct)
        {
            Product product = _fakeRepo.Products.FirstOrDefault(p => p.Id == id);

            if (product == null)
            {
                //return NotFound();
                return StatusCode(426);     // deliberate mistake, send wrong http code.
            }

            product.Name = updateProduct.Name;
            product.Price = updateProduct.Price;
            
            return Ok();
        }

        /// <summary>
        /// Deletes a product.
        /// </summary>
        /// <param name="id">The ID of the product to delete.</param>
        /// <returns>Http 200 response if successful.</returns>
        [HttpDelete("product/{id}")]
        public ActionResult Delete(int id)
        {
            Product product = _fakeRepo.Products.FirstOrDefault(p => p.Id == id);

            if (product == null)
            {
                return NotFound();
            }

            _fakeRepo.Products.Remove(product);
            return Ok();
        }
    }
}