﻿using System.Collections.Generic;

namespace QATest.DTOs
{
    public static class Mapper
    {
        public static List<DtoProduct> ProductsToDtos(List<Models.Product> products)
        {
            List<DtoProduct> dtoProducts = new List<DtoProduct>();
            products.ForEach(p => dtoProducts.Add(ProductToDto(p)));
            return dtoProducts;
        }

        public static DtoProduct ProductToDto(Models.Product product)
        {
            return new DtoProduct
            {
                Id = product.Id,
                Name = product.Name,
                Price = product.Price.ToString("F")
            };
        }
    }
}
