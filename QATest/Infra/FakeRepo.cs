﻿using System.Collections.Generic;
using QATest.Models;

namespace QATest.Infra
{
    public class FakeRepo
    {
        private List<Product> _products;

        public FakeRepo()
        {
            _products = new List<Product>();

            _products.AddRange(new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Lavender heart",
                    Price = 9.25m
                },
                new Product
                {
                    Id = 2,
                    Name = "Personalised cufflinks",
                    Price = 45.00m
                },
                new Product
                {
                    Id = 3,
                    Name = "Kids T-shirt",
                    Price = 19.95m
                }
            });
        }

        public List<Product> Products
        {
            get
            {
                return _products;
            }
        }
    }
}
