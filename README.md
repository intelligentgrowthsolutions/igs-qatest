# IGS QA Engineer Tech Test

This is the source code for the QA test. The runtime files needed, as well as the README test instructions, can be found in:

```
bin\Release\netcoreapp2.2\publish
```

Use the following command to run the project:
```
dotnet .\QATest.dll
```